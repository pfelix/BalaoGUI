﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BalaoGUI
{
    public class Balao
    {

        #region Atributos

        private string _cor;
        private string _direcao;
        private int _altura;


        #endregion

        #region Propriedades

        public string Cor { get; set; }
        public string Direcao { get; set; }
        public int Altura { get; set; }

        #endregion

        #region Construtores

        /// <summary>
        /// Contrutor - Default
        /// </summary>
        public Balao () : this ("Vermelho", "Subir", 0) { }

        /// <summary>
        /// Construtor - Parametros
        /// </summary>
        /// <param name="cor"></param>
        /// <param name="direcao"></param>
        /// <param name="altura"></param>
        public Balao (string cor, string direcao, int altura)
        {
            Cor = cor;
            Direcao = direcao;
            Altura = altura;

        }

        /// <summary>
        /// Construtor - Cópia
        /// </summary>
        /// <param name="balao"></param>
        public Balao (Balao balao)
        {
            Cor = balao.Cor;
            Direcao = balao.Direcao;
            Altura = balao.Altura;

        }

        #endregion

        #region Metodos Outros

        public int subir (int valor)
        {
            Altura += valor;

            return Altura;
        }

        public int descer (int valor)
        {
            if (Altura > 0)
            {
                Altura -= valor;

            }
            return valor;
        }

        #endregion

        #region Metodos Gerais

        #endregion
    }
}
