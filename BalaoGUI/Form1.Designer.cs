﻿namespace BalaoGUI
{
    partial class Form1
    {
        /// <summary>
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Windows Form Designer

        /// <summary>
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.ButtonIniciar = new System.Windows.Forms.Button();
            this.ButtonSubir = new System.Windows.Forms.Button();
            this.ButtonDescer = new System.Windows.Forms.Button();
            this.LabelStatus = new System.Windows.Forms.Label();
            this.ComboBoxCor = new System.Windows.Forms.ComboBox();
            this.LabelCor = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // ButtonIniciar
            // 
            this.ButtonIniciar.Location = new System.Drawing.Point(188, 12);
            this.ButtonIniciar.Name = "ButtonIniciar";
            this.ButtonIniciar.Size = new System.Drawing.Size(75, 23);
            this.ButtonIniciar.TabIndex = 0;
            this.ButtonIniciar.Text = "Iniciar...";
            this.ButtonIniciar.UseVisualStyleBackColor = true;
            this.ButtonIniciar.Click += new System.EventHandler(this.ButtonIniciar_Click);
            // 
            // ButtonSubir
            // 
            this.ButtonSubir.Enabled = false;
            this.ButtonSubir.Location = new System.Drawing.Point(12, 88);
            this.ButtonSubir.Name = "ButtonSubir";
            this.ButtonSubir.Size = new System.Drawing.Size(75, 23);
            this.ButtonSubir.TabIndex = 1;
            this.ButtonSubir.Text = "Subir";
            this.ButtonSubir.UseVisualStyleBackColor = true;
            this.ButtonSubir.Click += new System.EventHandler(this.ButtonSubir_Click);
            // 
            // ButtonDescer
            // 
            this.ButtonDescer.Enabled = false;
            this.ButtonDescer.Location = new System.Drawing.Point(12, 151);
            this.ButtonDescer.Name = "ButtonDescer";
            this.ButtonDescer.Size = new System.Drawing.Size(75, 23);
            this.ButtonDescer.TabIndex = 2;
            this.ButtonDescer.Text = "Descer";
            this.ButtonDescer.UseVisualStyleBackColor = true;
            this.ButtonDescer.Click += new System.EventHandler(this.ButtonDescer_Click);
            // 
            // LabelStatus
            // 
            this.LabelStatus.AutoSize = true;
            this.LabelStatus.Location = new System.Drawing.Point(47, 36);
            this.LabelStatus.Name = "LabelStatus";
            this.LabelStatus.Size = new System.Drawing.Size(37, 13);
            this.LabelStatus.TabIndex = 3;
            this.LabelStatus.Text = "Status";
            // 
            // ComboBoxCor
            // 
            this.ComboBoxCor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBoxCor.FormattingEnabled = true;
            this.ComboBoxCor.Items.AddRange(new object[] {
            "Vermelho",
            "Amarelo",
            "Verde"});
            this.ComboBoxCor.Location = new System.Drawing.Point(50, 12);
            this.ComboBoxCor.Name = "ComboBoxCor";
            this.ComboBoxCor.Size = new System.Drawing.Size(121, 21);
            this.ComboBoxCor.TabIndex = 4;
            // 
            // LabelCor
            // 
            this.LabelCor.AutoSize = true;
            this.LabelCor.Location = new System.Drawing.Point(18, 15);
            this.LabelCor.Name = "LabelCor";
            this.LabelCor.Size = new System.Drawing.Size(23, 13);
            this.LabelCor.TabIndex = 5;
            this.LabelCor.Text = "Cor";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(95, 56);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(168, 158);
            this.pictureBox1.TabIndex = 6;
            this.pictureBox1.TabStop = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(275, 226);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.LabelCor);
            this.Controls.Add(this.ComboBoxCor);
            this.Controls.Add(this.LabelStatus);
            this.Controls.Add(this.ButtonDescer);
            this.Controls.Add(this.ButtonSubir);
            this.Controls.Add(this.ButtonIniciar);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button ButtonIniciar;
        private System.Windows.Forms.Button ButtonSubir;
        private System.Windows.Forms.Button ButtonDescer;
        private System.Windows.Forms.Label LabelStatus;
        private System.Windows.Forms.ComboBox ComboBoxCor;
        private System.Windows.Forms.Label LabelCor;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}

