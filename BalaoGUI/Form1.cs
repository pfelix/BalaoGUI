﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BalaoGUI
{
    public partial class Form1 : Form
    {
        Balao myBalao;

        public Form1()
        {
            InitializeComponent();
            LabelStatus.Text = "";
        }

        private void ButtonIniciar_Click(object sender, EventArgs e)
        {
            if (ButtonIniciar.Text == "Iniciar...")
            {
                if (!(ComboBoxCor.Text == ""))
                {
                    myBalao = new Balao(ComboBoxCor.Text, "Subir", 0);
                    LabelStatus.Text = "O balão está no chão.";
                    ButtonIniciar.Text = "Parar...";
                    ButtonDescer.Enabled = true;
                    ButtonSubir.Enabled = true;
                }
                else
                {
                    LabelStatus.Text = "Erro: Falta escolher a cor.";
                }
            }
            else
            {
                LabelStatus.Text = "";
                ButtonIniciar.Text = "Iniciar...";
                ButtonDescer.Enabled = false;
                ButtonSubir.Enabled = false;
            }
        }

        private void ButtonSubir_Click(object sender, EventArgs e)
        {
            myBalao.subir(1);
            LabelStatus.Text = "O balão está a " + myBalao.Altura + " metros do solo.";
        }

        private void ButtonDescer_Click(object sender, EventArgs e)
        {
            myBalao.descer(1);
            if (myBalao.Altura == 0)
            {
                LabelStatus.Text = "O balão está no chão.";
            }
            else
            {
                LabelStatus.Text = "O balão está a " + myBalao.Altura + " metros do solo.";
            }
        }
    }
}
